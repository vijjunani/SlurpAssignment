# Slurp Assignment

This is the programming assignment to slurp for the role of Junior software developer.

## Environment
1. Operating System: Windows 10 64-bit
2. Programming Language: C#, .Net Framework 4.6.1
3. IDE: Visual Studio 2017

## How to read the solution
1. Open the solution "SlurpAssignment.sln" from the SlurpAssignment folder through Visual Studio.
2. The solution contains two projects:
    1. SlurpConsoleApp -> C# console application
        This application contains the source code for the task.

        Things done:
        1. Created three model classes, each for Coffeebag, Cubic Box & Order.
        2. A static class to compute the boxes required.

    2. SlurpConsoleTest -> unit test project
        This is a unit test project for the application

## How to run the solution
1. Open the solution "SlurpAssignment.sln" from the SlurpAssignment folder through Visual Studio.
2. Run the application (Ctrl + F5)

## Results

Example1:

```
No. of units of Bag  200g: 21
No. of units of Bag  400g: 20
No. of units of Bag  1000g: 10
Length of an edge of cubicbox: 40
Number of boxes required: 2
Place next order (1 to continue, 0 to exit):
```

Example2:

```
No. of units of Bag  200g: -12
Not a valid number, try again. Enter a positive integer
12
No. of units of Bag  400g: ab
Not a valid number, try again. Enter a positive integer
10
No. of units of Bag  1000g: 1
Length og an edge of cubicbox: 40
Number of boxes required: 1
Place next order (1 to continue, 0 to exit):
```