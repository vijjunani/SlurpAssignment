﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SlurpConsoleApp {

    // Model for Coffee bag
    public class CoffeeBag {
        private uint _width, _height, _length;

        public CoffeeBag(uint width, uint height, uint length) {
            Width = width; Height = height; Length = length;
        }

        public uint Height { get => _height; set => _height = value; }

        public uint Length { get => _length; set => _length = value; }

        public uint Width { get => _width; set => _width = value; }

        public uint Volume {
            get { return Width * Height * Length; }
        }
    }

    // Model for CubicBox
    public class CubicBox {
        private uint _side;

        public CubicBox(uint side) {
            Side = side;
        }

        public uint Side { get => _side; set => _side = value; }

        public uint Volume {
            get { return Side * Side * Side; }
        }
    }

    // Model for order
    public class Order {
        private IDictionary<CoffeeBag, uint> _order;

        public Order(IDictionary<CoffeeBag, uint> order) {
            _order = order;
        }

        public bool IsValid {
            get {
                return _order.Count > 0 && _order.Values.Any(x => x > 0);
            }
        }

        public uint Volume {
            get {
                uint combinedvolume = 0;
                foreach (var item in _order)
                    combinedvolume += item.Key.Volume * item.Value;
                return combinedvolume;
            }
        }
    }

    // Computation logic
    public static class SlurpCompute {
        public const string ERROR_INVALID_NUMBER = "Not a valid number, try again. Enter a positive integer";
        public const string ERROR_INVALID_CUBE_EDGE = "Not a valid number, try again. Enter a positive integer in range 30-100";

        public static readonly CoffeeBag bag1000 = new CoffeeBag(14, 26, 10);
        public static readonly CoffeeBag bag200 = new CoffeeBag(16, 23, 2);
        public static readonly CoffeeBag bag400 = new CoffeeBag(22, 26, 2);

        public static uint CalculateNumberOfBoxes(Order order, CubicBox cubicBox) {
            uint boxes = 0;
            try {
                boxes = (uint)Math.Ceiling((decimal)order.Volume / cubicBox.Volume);
            } catch (DivideByZeroException e) {
                throw new DivideByZeroException("Failed with exception: " + e.Message);
            } catch (Exception e) {
                throw new Exception("Failed with exception: " + e.Message);
            }
            return boxes;
        }
    }
}