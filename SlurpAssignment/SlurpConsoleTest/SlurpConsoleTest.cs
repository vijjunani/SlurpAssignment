﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SlurpConsoleApp;
using System;
using System.Collections.Generic;

namespace SlurpConsoleTest {

    [TestClass]
    public class SlurpConsoleTest {
        [TestMethod]
        public void OrderWithLegitimateValues() {
            var coffeeBagOrder = new Dictionary<CoffeeBag, uint>() {
                { SlurpCompute.bag200, 10 }, { SlurpCompute.bag400, 3 }, { SlurpCompute.bag1000, 21 }
            };
            var order = new Order(coffeeBagOrder);
            var cubicBox = new CubicBox(30);
            var actualValue = SlurpCompute.CalculateNumberOfBoxes(order, cubicBox);

            Assert.IsTrue(actualValue > 0);
        }

        [TestMethod]
        public void OrderWithZeroBags() {
            var coffeeBagOrder = new Dictionary<CoffeeBag, uint>() {
                { SlurpCompute.bag200, 0 }, { SlurpCompute.bag400, 0 }, { SlurpCompute.bag1000, 0 }
            };
            var order = new Order(coffeeBagOrder);
            var cubicBox = new CubicBox(70);
            var value = SlurpCompute.CalculateNumberOfBoxes(order, cubicBox);

            Assert.IsTrue(value == 0);
        }

        [TestMethod]
        public void OrderWithZeroBoxSize() {
            var coffeeBagOrder = new Dictionary<CoffeeBag, uint>() {
                { SlurpCompute.bag200, 10 }, { SlurpCompute.bag400, 3 }, { SlurpCompute.bag1000, 21 }
            };
            var order = new Order(coffeeBagOrder);
            var cubicBox = new CubicBox(0);

            Assert.ThrowsException<DivideByZeroException>(() => SlurpCompute.CalculateNumberOfBoxes(order, cubicBox));
        }
    }
}